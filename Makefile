#  OpenUH Compiler
#CC = uhcc
#CFLAGS = -openmp -O3 -std=c99


#  Open64 Compiler
#CC = opencc
#CFLAGS = -openmp -O3 -std=c99


# Intel C Compiler
#CC = icc
#CFLAGS = -openmp -O3 -std=c99


# Gnu C Compiler
CC = g++
CFLAGS = -fopenmp -O3 -g -pedantic -std=c++11
FLAG = -std=c++11



# PGI C Compiler
# CC = pgcc
# CFLAGS = -mp -O3


# Sun Studio C Compiler
# CC = suncc 
# CFLAGS = -fast -xopenmp

BIN=./bin

LIBS = -lm
LDFLAGS = $(CFLAGS)
.PHONY: all bin tidy clean cleanlogs task_bench taskwait_bench \
	task_firstpriv_bench

SOURCES=task_bench.cpp taskwait_bench.cpp task_firstpriv_bench.cpp 
OBJECTS=$(SOURCES:.cpp=.o)
OTHEROBJS=common.o ProfileTracker.o 

all: bin task_bench taskwait_bench task_firstpriv_bench 

bin:
	mkdir -p $(BIN)

task_bench:  $(OTHEROBJS) task_bench.o 
	$(CC) $^ -o $(BIN)/$@_$(CC) $(LDFLAGS) $(LIBS) 

taskwait_bench: taskwait_bench.o  $(OTHEROBJS) task_bench.o
	$(CC) $^ -o $(BIN)/$@_$(CC) $(LDFLAGS) $(LIBS) 

task_firstpriv_bench: task_firstpriv_bench.o  $(OTHEROBJS) 
	$(CC) $^ -o $(BIN)/$@_$(CC) $(LDFLAGS) $(LIBS) 

tidy: 
	rm -f *.o 

clean:tidy 
	rm -f $(BIN)/*

